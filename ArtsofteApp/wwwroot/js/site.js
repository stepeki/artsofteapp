﻿$(document).on('click', '#submitEmployee', function () {
  var form = $('#employeeForm');
  var valid = form.valid();
  if (!valid) {
    return;
  };
  var model = getFormData(form);
  
  $.ajax({
    url: '/employee/save',
    data: {
      employee: model,
      isPartial:true
    },
    type:'post',
    success: function (response) {
      $('.body-content').html(response);
      setLocation("/");
    }
  });
});
$(document).on('click', '.deleteEmployee', function () {
  var id = $(this).data('id');
  $.ajax({
    url: '/employee/delete',
    data: {
      id: id,
      isPartial: true
    },
    success: function (response) {
      $('.body-content').html(response);
      setLocation("/");
    }
  });
});
$(function () {
  var nameInput = $("#firstNameEmployeeForm");
  var names = nameInput.data('names');
  $("#firstNameEmployeeForm").autocomplete({
    source: names.split(',')
  });

})


//#region Extentions
jQuery.extend(jQuery.validator.messages, {
  required: "Поле обязательно к заполнению",
  number: "Введите число",
  max: jQuery.validator.format("В России столько не живут. Выберите число , меньшее {0}."),
  min: jQuery.validator.format("Данное поле должно быть не меньше, чем {0}. (ну, у вас же не работают дети) :)")
});
//#endregion

//#region Functions
function getFormData($form) {
  var unindexed_array = $form.serializeArray();
  var indexed_array = {};

  $.map(unindexed_array, function (n, i) {
    indexed_array[n['name']] = n['value'];
  });

  return indexed_array;
}
function setLocation(curLoc) {
  try {
    history.pushState(null, null, curLoc);
    return;
  } catch (e) {
    console.log(e);
  }
  location.hash = '#' + curLoc;
}
//#endregion