﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Web.Models;

namespace Web.Controllers
{
    public class DepartmentController : BaseController
    {
        public DepartmentController(DataContext _context) : base(_context)
        {
        }
        /// <summary>
        /// Список отделов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index(bool isPartial = false)
        {
            var langs = GetDepartments();
            if (isPartial)
                return PartialView(langs);
            else
            {
                return View(langs);
            }
        }
        /// <summary>
        /// Добавить/Изменить ЯП
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult EditOrCreate(int? id, bool isPartial = false)
        {
            var department = id.HasValue ? context.Departments.First(x => id.Value == x.Id) : new Department();

            if (isPartial)
                return PartialView(department);
            else
            {
                return View(department);
            }
        }
       
        /// <summary>
        /// Удаление ЯП
        /// </summary>
        /// <param name="id">Id ЯП</param>
        /// <param name="isPartial"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Delete(int id, bool isPartial = false)
        {
            var department = context.Departments.Include("Employees").First(x => x.Id == id);
            foreach(var employee in department.Employees)
            {
                employee.DepartmentId = null;
            }
            context.Departments.Remove(department);
            context.SaveChanges();
            var result = GetDepartments();
            return RedirectToAction("Index");
        }
        /// <summary>
        /// Сохранение ЯП
        /// </summary>
        /// <param name="employee">Модель</param>
        /// <param name="isPartial">Какой нужен результат</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Save(Department department)
        {
            if (department.Id == 0)
            {
                context.Departments.Add(department);
            }
            else
            {
                var edit = context.Departments.FirstOrDefault(x => x.Id == department.Id);
                edit.Title = department.Title;
                edit.Floor = department.Floor;
            }
            var res = context.SaveChanges();
            return RedirectToAction("Index");
        }
        #region Private
        private IEnumerable<Department> GetDepartments()
        {
            var res = context.Departments.Include("Employees");
            return res;
        }
        #endregion
    }
}
