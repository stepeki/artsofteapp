﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DataLayer;
namespace Web.Controllers
{
    /// <summary>
    /// Для общих действий всех контроллеров
    /// </summary>
    public class BaseController : Controller
    {        
        internal DataContext context { get; set; }
        public BaseController(DataContext _context)
        {
            context = _context;
        }

    }
}