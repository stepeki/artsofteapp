﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Web.Models;

namespace Web.Controllers
{
    public class LanguageController : BaseController
    {
        public LanguageController(DataContext _context) : base(_context)
        {
        }
        /// <summary>
        /// Список ЯП
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index(bool isPartial = false)
        {
            var langs = GetLanguages();
            if (isPartial)
                return PartialView(langs);
            else
            {
                return View(langs);
            }
        }
        /// <summary>
        /// Добавить/Изменить ЯП
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult EditOrCreate(int? id, bool isPartial = false)
        {
            var language = id.HasValue ? context.ProgrammingLanguages.First(x => id.Value == x.Id) : new ProgrammingLanguage();

            if (isPartial)
                return PartialView(language);
            else
            {
                return View(language);
            }
        }
       
        /// <summary>
        /// Удаление ЯП
        /// </summary>
        /// <param name="id">Id ЯП</param>
        /// <param name="isPartial"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Delete(int id, bool isPartial = false)
        {
            var lang = context.ProgrammingLanguages.Include("Employees").First(x => x.Id == id);
            foreach(var employee in lang.Employees)
            {
                employee.ProgrammingLanguageId = null;
            }
            context.ProgrammingLanguages.Remove(lang);
            context.SaveChanges();
            var result = GetLanguages();
            return RedirectToAction("Index");
        }
        /// <summary>
        /// Сохранение ЯП
        /// </summary>
        /// <param name="employee">Модель</param>
        /// <param name="isPartial">Какой нужен результат</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Save(ProgrammingLanguage language)
        {
            if (language.Id == 0)
            {
                context.ProgrammingLanguages.Add(language);
            }
            else
            {
                var edit = context.ProgrammingLanguages.FirstOrDefault(x => x.Id == language.Id);
                edit.Title = language.Title;
            }
            var res = context.SaveChanges();
            return RedirectToAction("Index");
        }
        #region Private
        private IEnumerable<ProgrammingLanguage> GetLanguages()
        {
            var res = context.ProgrammingLanguages.Include("Employees");
            return res;
        }
        #endregion
    }
}
