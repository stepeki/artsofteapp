﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Web.Models;

namespace Web.Controllers
{
    public class EmployeeController : BaseController
    {
        public EmployeeController(DataContext _context) : base(_context)
        {
        }
        /// <summary>
        /// Список сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index(bool isPartial = false)
        {
            var employees = GetEmployees();
            if (isPartial)
                return PartialView(employees);
            else
            {
                return View(employees);
            }
        }
        /// <summary>
        /// Добавить сотрудника(используется та же вьюшка, чтоб например в меньших местах менять стили. 
        /// Если использовать разные целенаправленно-проще копировать, чем изначально менять в двух местах)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Add(bool isPartial = false)
        {
            var employee = new Employee();
            var departments = context.Departments.ToList();
            var programmingLanguages = context.ProgrammingLanguages.ToList();

            var viewModel = new CreateEmployeeViewModel
            {
                Departments = departments,
                Employee = employee,
                ProgrammingLanguages = programmingLanguages
            };

            InitAutocomplete();

            if (isPartial)
                return PartialView("Edit", viewModel);
            else
            {
                return View("Edit", viewModel);
            }
        }
        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Edit(int id, bool isPartial = false)
        {
            var employee = context.Employes.First(x => x.Id == id);
            var departments = context.Departments.ToList();
            var programmingLanguages = context.ProgrammingLanguages.ToList();

            var viewModel = new CreateEmployeeViewModel
            {
                Departments = departments,
                Employee = employee,
                ProgrammingLanguages = programmingLanguages
            };

            InitAutocomplete();

            if (isPartial)
                return PartialView("Edit", viewModel);
            else
            {
                return View("Edit", viewModel);
            }
        }
        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <param name="isPartial"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Delete(int id, bool isPartial = false)
        {
            var employee = context.Employes.First(x => x.Id == id);
            context.Employes.Remove(employee);
            context.SaveChanges();

            var result = GetEmployees();
            if (isPartial)
                return PartialView("Index", result);
            else
            {
                return View("Index", result);
            }
        }
        /// <summary>
        /// Сохранение сотрудника
        /// </summary>
        /// <param name="employee">Модель</param>
        /// <param name="isPartial">Какой нужен результат</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Save(Employee employee, bool isPartial = false)
        {
            if(employee.Id == 0)
            {
                context.Employes.Add(employee);
            }
            else
            {
                var edit = context.Employes.FirstOrDefault(x => x.Id == employee.Id);
                edit.FirstName = employee.FirstName;
                edit.LastName = employee.LastName;
                edit.Age = employee.Age;
                edit.IsMale = employee.IsMale;
                edit.DepartmentId = employee.DepartmentId;
                edit.ProgrammingLanguageId = employee.ProgrammingLanguageId;
            }
            var res = context.SaveChanges();
            var employees = GetEmployees();
            if (isPartial)
            {
                return PartialView("Index", employees);
            }
            else
            {
                return View("Index", employees);
            }
        }
        #region Private
        private IEnumerable<Employee> GetEmployees()
        {
            var res = context.Employes.Include("Department").Include("ProgrammingLanguage");
            return res;
        }
        private void InitAutocomplete()
        {
            ViewBag.Autocomplete = context.Settings.FirstOrDefault(x => x.Key.ToLower() == "autocomplete")?.Value ?? "";
        }
        #endregion
    }
}
