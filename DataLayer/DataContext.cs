﻿using DataLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataLayer
{
    /// <summary>
    /// Контекст для работы с БД
    /// </summary>
    public class DataContext : DbContext
    {
        /// <summary>
        /// Работники
        /// </summary>
        public DbSet<Employee> Employes { get; set; }
        /// <summary>
        /// Отделы
        /// </summary>
        public DbSet<Department> Departments { get; set; }
        /// <summary>
        /// Языки программирования
        /// </summary>
        public DbSet<ProgrammingLanguage> ProgrammingLanguages { get; set; }
        /// <summary>
        /// Настройки
        /// </summary>
        public DbSet<Setting> Settings { get; set; }
        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
        }

    }
}
