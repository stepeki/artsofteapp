﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DataLayer.Models
{
    /// <summary>
    /// Модель работника
    /// </summary>
    public class Employee : Entity
    {
        /// <summary>
        /// Имя
        /// </summary>
        [DisplayName("Имя")]
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        [DisplayName("Фамилия")]
        public string LastName {get;set;}
        /// <summary>
        /// Возраст
        /// </summary>
        [DisplayName("Возраст")]
        public int Age { get; set; }
        /// <summary>
        /// Это мужик?
        /// </summary>
        [DisplayName("Мужчина?")]
        public bool IsMale { get; set; }
        /// <summary>
        /// Отдел
        /// </summary>
        [DisplayName("Отдел")]
        public virtual Department Department { get; set; }
        /// <summary>
        /// Id отдела
        /// </summary>
        [DisplayName("Отдел")]
        public int? DepartmentId { get; set; }
        /// <summary>
        /// Язык программирования
        /// </summary>
        [DisplayName("Язык программирования")]
        public virtual ProgrammingLanguage ProgrammingLanguage { get; set; }
        /// <summary>
        /// ID языка программирования
        /// </summary>
        [DisplayName("Язык программирования")]
        public int? ProgrammingLanguageId { get; set; }
    }
}
