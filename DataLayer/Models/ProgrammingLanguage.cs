﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    /// <summary>
    /// Язык программирования
    /// </summary>
    public class ProgrammingLanguage:Entity
    {
        /// <summary>
        /// Название языка программирования
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Работники знающие этот язык
        /// </summary>
        public IEnumerable<Employee> Employees { get; set; }
    }
}
