﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    /// <summary>
    /// Отдел
    /// </summary>
    public class Department:Entity
    {
        /// <summary>
        /// Название отдела
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Этаж отдела
        /// </summary>
        public int Floor { get; set; }
        /// <summary>
        /// Работники в данном отделе
        /// </summary>
        public IEnumerable<Employee> Employees { get; set; }
    }
}
