﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    /// <summary>
    /// Общие поля для всех сущностей в БД
    /// </summary>
    public class Entity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }
    }
}
