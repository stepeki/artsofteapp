﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataLayer.Migrations
{
    public partial class Added_ProgrammingLanguage_ForeignKey_To_Employee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employes_Departments_DepartmentId",
                table: "Employes");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentId",
                table: "Employes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "ProgrammingLanguageId",
                table: "Employes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employes_ProgrammingLanguageId",
                table: "Employes",
                column: "ProgrammingLanguageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employes_Departments_DepartmentId",
                table: "Employes",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employes_ProgrammingLanguages_ProgrammingLanguageId",
                table: "Employes",
                column: "ProgrammingLanguageId",
                principalTable: "ProgrammingLanguages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employes_Departments_DepartmentId",
                table: "Employes");

            migrationBuilder.DropForeignKey(
                name: "FK_Employes_ProgrammingLanguages_ProgrammingLanguageId",
                table: "Employes");

            migrationBuilder.DropIndex(
                name: "IX_Employes_ProgrammingLanguageId",
                table: "Employes");

            migrationBuilder.DropColumn(
                name: "ProgrammingLanguageId",
                table: "Employes");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentId",
                table: "Employes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Employes_Departments_DepartmentId",
                table: "Employes",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
